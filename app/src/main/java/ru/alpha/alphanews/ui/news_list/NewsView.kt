package ru.alpha.alphanews.ui.news_list

import ru.alpha.alphanews.data.news.NewsItem

/**
 * Created by Aleksandr Sobol
 * asobol@golamago.com
 * on 24/06/2018.
 */
interface NewsView {

    fun showNews(news: List<NewsItem>)
    fun showLoading(show: Boolean)
    fun showError()

}