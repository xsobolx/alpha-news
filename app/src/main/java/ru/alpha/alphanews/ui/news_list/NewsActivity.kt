package ru.alpha.alphanews.ui.news_list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_news.*
import ru.alpha.alphanews.R
import ru.alpha.alphanews.data.NewsInteractor
import ru.alpha.alphanews.data.network.AlphaNewsService
import ru.alpha.alphanews.data.network.ApiFactory
import ru.alpha.alphanews.data.news.NewsItem
import ru.alpha.alphanews.ui.news_description.NewsDescriptionActivity


/**
 * Created by Aleksandr Sobol
 * asobol@golamago.com
 * on 23/06/2018.
 */
class NewsActivity : AppCompatActivity(), NewsView {

    private var presenter: NewsPresenter? = null

    private var adapter: NewsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        if (presenter == null) {
            initPresenter()
        }

        adapter = NewsAdapter(object : NewsAdapter.OnItemClickListener {
            override fun onItemClick(newsItem: NewsItem) {
                startActivity(NewsDescriptionActivity.startIntent(applicationContext, newsItem.link))
            }
        })

        val llm = LinearLayoutManager(this)

        news_recycler.layoutManager = llm
        news_recycler.adapter = adapter

        refresh.setOnRefreshListener { presenter?.onRefresh() }

        presenter?.onAttach(this)
    }

    override fun showNews(news: List<NewsItem>) {
        adapter?.setData(news)
    }

    override fun showLoading(show: Boolean) {
        refresh.isRefreshing = show
    }

    override fun showError() {
        Toast.makeText(this, R.string.loading_news_error, Toast.LENGTH_LONG).show()
    }

    private fun initPresenter() {
        val client = ApiFactory.buildClient()
        val retrofit = ApiFactory.buildRetrofit(client)
        val service = AlphaNewsService.create(retrofit)
        val interactor = NewsInteractor.Impl(service)
        presenter = NewsPresenter(interactor)
    }
}