package ru.alpha.alphanews.ui.news_list

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.alpha.alphanews.data.NewsInteractor
import ru.alpha.alphanews.data.news.NewsItem

/**
 * Created by Aleksandr Sobol
 * asobol@golamago.com
 * on 24/06/2018.
 */
class NewsPresenter(private val newsInteractor: NewsInteractor) {

    private var view: NewsView? = null
    private var getNewsDisposable: Disposable? = null

    fun onAttach(view: NewsView) {
        this.view = view

        getNews(view)
    }

    fun onRefresh() {
        getNews(view)
    }

    private fun getNews(view: NewsView?) {
        getNewsDisposable?.let { disposable ->
            if (!disposable.isDisposed) {
                disposable.dispose()
            }
        }

        getNewsDisposable = newsInteractor.getNews()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view?.showLoading(true) }
                .doFinally { view?.showLoading(false) }
                .subscribe({ news: List<NewsItem> -> view?.showNews(news) },
                        { _: Throwable -> view?.showError() })
    }
}