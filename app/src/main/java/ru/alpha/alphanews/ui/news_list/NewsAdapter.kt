package ru.alpha.alphanews.ui.news_list

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import ru.alpha.alphanews.R
import ru.alpha.alphanews.data.news.NewsItem


/**
 * Created by Aleksandr Sobol
 * asobol@golamago.com
 * on 23/06/2018.
 */
class NewsAdapter(private val listener: OnItemClickListener) : RecyclerView.Adapter<NewsAdapter.RssNewsHolder>() {

    private var newsItems: List<NewsItem> = ArrayList()

    interface OnItemClickListener {
        fun onItemClick(newsItem: NewsItem)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RssNewsHolder {
        val layoutInflater = LayoutInflater.from(viewGroup.context)
        val v = layoutInflater.inflate(R.layout.rss_feed_item, viewGroup, false)
        return RssNewsHolder(v, listener)
    }

    fun setData(newsItems: List<NewsItem>) {
        this.newsItems = newsItems
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RssNewsHolder, pos: Int) {
        holder.bind(newsItems[pos])
    }

    override fun getItemCount(): Int = newsItems.size

    class RssNewsHolder(itemView: View,
                        private val listener: OnItemClickListener) : RecyclerView.ViewHolder(itemView) {
        private val titleTextField: TextView
        private val descriptionTextField: TextView
        private val container: LinearLayout
        private val publicationDateTextField: TextView

        init {
            container = itemView.findViewById(R.id.container)
            titleTextField = itemView.findViewById(R.id.title)
            descriptionTextField = itemView.findViewById(R.id.description)
            publicationDateTextField = itemView.findViewById(R.id.pubdate)
        }

        fun bind(newsItem: NewsItem) {
            titleTextField.text = newsItem.title
            publicationDateTextField.text = newsItem.pubDate
            descriptionTextField.text = Html.fromHtml(newsItem.description, Html.FROM_HTML_MODE_COMPACT)
            container.setOnClickListener { listener.onItemClick(newsItem) }
        }
    }
}