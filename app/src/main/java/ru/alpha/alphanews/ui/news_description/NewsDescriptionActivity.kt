package ru.alpha.alphanews.ui.news_description

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_news_description.*
import ru.alpha.alphanews.R

/**
 * Created by Aleksandr Sobol
 * asobol@golamago.com
 * on 24/06/2018.
 */
class NewsDescriptionActivity : AppCompatActivity() {

    companion object {
        const val NEWS_DESCRIPTION_LINK_EXTRA = "news_description_link_extra"

        fun startIntent(context: Context, newsDescriptionUrl: String) : Intent {
            val intent = Intent(context, NewsDescriptionActivity::class.java)
            intent.putExtra(NEWS_DESCRIPTION_LINK_EXTRA, newsDescriptionUrl)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_description)

        val newsDescriptionUrl = intent.extras.getString(NEWS_DESCRIPTION_LINK_EXTRA)

        news_web_view.loadUrl(newsDescriptionUrl)
    }

}