package ru.alpha.alphanews.data.network

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import ru.alpha.alphanews.data.news.Rss

/**
 * Created by Aleksandr Sobol
 * asobol@golamago.com
 * on 23/06/2018.
 */
interface AlphaNewsService {

    companion object Factory {
        fun create(retrofit: Retrofit): AlphaNewsService =
                retrofit.create<AlphaNewsService>(AlphaNewsService::class.java)
    }

    @GET("_/rss/_rss.html")
    fun getNews(@Query("subtype") subtype: Int,
                @Query("category") category: Int,
                @Query("city") city: Int): Single<Rss>

}