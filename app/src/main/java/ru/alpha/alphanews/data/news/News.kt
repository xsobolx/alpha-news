package ru.alpha.alphanews.data.news

import org.simpleframework.xml.*
import org.simpleframework.xml.ElementList


/**
 * Created by Aleksandr Sobol
 * asobol@golamago.com
 * on 23/06/2018.
 */
@Root(name = "rss", strict = false)
data class Rss @JvmOverloads constructor (
        @field:Element(name = "channel")
        @param:Element(name = "channel")
        val channel: Channel
)

@Root(strict = false, name = "channel")
data class Channel @JvmOverloads constructor(
        @param:Element(name = "title")
        @field:Element(name = "title")
        val title: String,
        @param:Element(name = "link")
        @field:Element(name = "link")
        val link: String,
        @param:Element(name = "description")
        @field:Element(name = "description")
        val description: String,
        @param:Element(name = "language")
        @field:Element(name = "language")
        val language: String,
        @param:Element(name = "pubDate")
        @field:Element(name = "pubDate")
        val pubDate: String,
        @param:Element(name = "docs")
        @field:Element(name = "docs")
        val docs: String,
        @param:Element(name = "managingEditor")
        @field:Element(name = "managingEditor")
        val managingEditor: String,
        @param:Element(name = "webMaster")
        @field:Element(name = "webMaster")
        val webMaster: String,
        @param:ElementList(name = "item", inline = true)
        @field:ElementList(name = "item", inline = true)
        val newsItems: List<NewsItem>
)

@Path("rss/channel")
@Root(strict = false, name = "item")
data class NewsItem @JvmOverloads constructor (
        @param:Element(name = "title")
        @field:Element(name = "title")
        val title: String,
        @param:Element(name = "link")
        @field:Element(name = "link")
        val link: String,
        @param:Element(name = "description")
        @field:Element(name = "description")
        val description: String,
        @param:Element(name = "pubDate")
        @field:Element(name = "pubDate")
        val pubDate: String,
        @param:Element(name = "guid")
        @field:Element(name = "guid")
        val guid: String
)

