package ru.alpha.alphanews.data

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ru.alpha.alphanews.data.network.AlphaNewsService
import ru.alpha.alphanews.data.news.NewsItem

/**
 * Created by Aleksandr Sobol
 * asobol@golamago.com
 * on 23/06/2018.
 */
interface NewsInteractor {

    fun getNews(subtype: Int = 1, category: Int = 2, city: Int = 21): Single<List<NewsItem>>

    class Impl(private val service: AlphaNewsService) : NewsInteractor {
        override fun getNews(subtype: Int, category: Int, city: Int): Single<List<NewsItem>> {
            return service.getNews(subtype, category, city)
                    .map { it.channel.newsItems }
                    .subscribeOn(Schedulers.io())
        }
    }
}